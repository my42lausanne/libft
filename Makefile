# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2021/11/24 14:41:15 by dfarhi           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS	= ft_atoi.c ft_bzero.c ft_calloc.c ft_isalnum.c ft_isalpha.c \
	ft_isascii.c ft_isdigit.c ft_isprint.c ft_itoa.c ft_memchr.c \
	ft_memcmp.c ft_memcpy.c ft_memmove.c ft_memset.c ft_putchar_fd.c \
	ft_putendl_fd.c ft_putnbr_fd.c ft_putstr_fd.c ft_split.c \
	ft_strchr.c ft_strdup.c ft_striteri.c ft_strjoin.c ft_strlcat.c \
	ft_strlcpy.c ft_strlen.c ft_strmapi.c ft_strncmp.c ft_strnstr.c \
	ft_strrchr.c ft_strtrim.c ft_substr.c ft_tolower.c ft_toupper.c

BONUSES	= ft_lstadd_back_bonus.c ft_lstadd_front_bonus.c \
	ft_lstclear_bonus.c ft_lstdelone_bonus.c ft_lstiter_bonus.c \
	ft_lstlast_bonus.c ft_lstmap_bonus.c ft_lstnew_bonus.c \
	ft_lstsize_bonus.c

OBJS	= ${SRCS:.c=.o}

BON_OBJ	= ${BONUSES:.c=.o}

HEADERS	= libft.h

NAME	= libft.a

CC		= gcc -Wall -Wextra -Werror

AR		= ar rcs

.c.o:
			${CC} -c $< -o ${<:.c=.o}

${NAME}:	${OBJS}
			${AR} ${NAME} ${OBJS}

all:		${NAME}

bonus:		${OBJS} ${BON_OBJ}
			${AR} ${NAME} ${OBJS} ${BON_OBJ}

clean:
			rm -f ${OBJS}
			rm -f ${BON_OBJ}

# "Force clean" remove all compiled files
fclean:		clean
			rm -f ${NAME}

# Rule to recompile
re:			fclean all

# Rules that do not have files, if those files exist, they are ignored
.PHONY:		all clean fclean re bonus
