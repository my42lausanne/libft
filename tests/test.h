/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 13:04:58 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/16 09:46:19 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
# define TEST_H

#include "../libft.h"
#include <stdio.h>
#include <unistd.h>

int	test_isalpha();
int	test_isdigit();
int	test_isalnum();
int	test_isascii();
int	test_isprint();
int	test_strlen();
int	test_memset();
int	test_bzero();
int	test_memcpy();
int	test_memmove();
int	test_strlcpy();
int	test_strlcat();
int	test_toupper();
int	test_tolower();
int	test_strchr();
int	test_strrchr();
int	test_strncmp();
int	test_memchr();
int	test_memcmp();
int	test_strnstr();
int	test_atoi();

int		is_compare(int a, int b);
int		s_strlcpy(char *dest, const char *src, int len);
//size_t	s_strlcat(char * restrict dst, const char * restrict src, size_t maxlen);
size_t	s_strlcat(char *dst, const char *src, size_t dstsize);
char	*s_strnstr(const char *s, const char *find, size_t slen);

#endif
