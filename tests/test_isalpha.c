/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isalpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:00:13 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/12 11:43:01 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <ctype.h>

int	test_isalpha(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 1000)
		if (!is_compare((int) isalpha(i), (int) ft_isalpha(i)))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", isalpha(i), ft_isalpha(i), i);
		}
	return (error);
}
