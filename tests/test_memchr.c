/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 11:32:01 by davifah           #+#    #+#             */
/*   Updated: 2022/01/03 15:36:22 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

static void	populate_s(char *s)
{
	int	c;

	ft_bzero(s, 100);
	c = -1;
	while (++c + 'a' < 'z')
		s[c] = c + 'a';
}

int	test_memchr(void)
{
	char	s[100];
	int		error;
	int 	i;
	int		n;
	int		end;

	end = 0;
	populate_s(s);
	error = 0;
	i = -1;
	while (++i + 'a' <= 'z' && !end)
	{
		n = -1;
		while (++n <= 30)
		{
			if (i + 'a' == 'z')
			{
				end = 1;
				i = -'a';
			}
			if (memchr(s, i + 'a', n) != ft_memchr(s, i + 'a', n))
			{
				error = 1;
				printf("ERROR char %c, len %d - og %p - yours %p - text %s\n", i + 'a', n, memchr(s, i + 'a', n), ft_memchr(s, i + 'a', n), (char *)ft_memchr(s, i + 'a', n));
			}
		}
	}
	return (error);
}
