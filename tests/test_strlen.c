/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 13:21:36 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/16 10:50:44 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>
#include <unistd.h>

int	test_strlen(void)
{
	int		error;
	char	str[100];

	error = 0;
	strcpy(str, "Teeeest");
	if ((unsigned int) ft_strlen(str) != strlen(str))
	{
		error = 1;
		printf("ERROR Str %s - og %lu - yours %lu\n", str, strlen(str), ft_strlen(str));
	}

	strcpy(str, "Teeeiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
	if ((unsigned int) ft_strlen(str) != strlen(str))
	{
		error = 1;
		printf("ERROR Str %s - og %lu - yours %lu\n", str, strlen(str), ft_strlen(str));
	}
	strcpy(str, "");
	if ((unsigned int) ft_strlen(str) != strlen(str))
	{
		error = 1;
		printf("ERROR Str %s - og %lu - yours %lu\n", str, strlen(str), ft_strlen(str));
	}
	return (error);
}
