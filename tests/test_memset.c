/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memset.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 14:09:50 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/11 14:30:57 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>
#include <unistd.h>

int	test_memset(void)
{
	char 	b[10];
	char 	a[10];
	int		c;
	int		len;
	int		error;

	error = 0;

	len = -1;
	while (++len < 10)
	{
		strcpy(a, "aaaaaaaaa");
		c = 'c';
		strcpy(b, a);
		memset(a, c, len);
		ft_memset(b, c, len);
		if (strcmp(a, b) != 0)
		{
			printf("ERROR c %c, len %d - og %s - yours %s", c, len, a, b);
		}
	}
	return (error);
}
