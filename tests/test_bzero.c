/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_bzero.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 14:09:50 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/11 14:44:56 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>
#include <unistd.h>

int	test_bzero(void)
{
	char 	b[10];
	char 	a[10];
	int		len;
	int		error;

	error = 0;

	len = -1;
	while (++len < 10)
	{
		strcpy(a, "aaaaaaaaa");
		strcpy(b, a);
		bzero(a, len);
		ft_bzero(b, len);
		if (strcmp(a, b) != 0)
		{
			printf("ERROR len %d - og %s - yours %s", len, a, b);
		}
	}
	return (error);
}
