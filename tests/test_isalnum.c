/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isalnum.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:36:59 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/12 11:36:31 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <ctype.h>

int	test_isalnum(void)
{
	int	error;
	int	i;

	error = 0;
	i = -1;
	while (++i <= 127)
		if (!is_compare((int) isalnum(i), (int) ft_isalnum(i)))
		{
			error = 1;
			printf("Error og %d - yours %d - char %c\n", isalnum(i), ft_isalnum(i), i);
		}
	return (error);
}
