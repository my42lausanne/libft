/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 16:36:54 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 16:59:07 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

static void	populate_s(char *s)
{
	int	c;

	ft_bzero(s, 100);
	c = -1;
	while (++c + 'a' < 'z')
		s[c] = c + 'a';
}

int	test_strchr(void)
{
	char	s[100];
	int		error;
	int 	i;
	int		end;

	end = 0;
	populate_s(s);
	error = 0;
	i = -1;
	while (++i + 'a' <= 'z' && !end)
	{
		if (i + 'a' == 'z')
		{
			end = 1;
			i = -'a';
		}
		if (strchr(s, i + 'a') != ft_strchr(s, i + 'a'))
		{
			error = 1;
			printf("ERROR char %c - og %p - yours %p - text %s\n", i + 'a', strchr(s, i + 'a'), ft_strchr(s, i + 'a'), ft_strchr(s, i + 'a'));
		}
	}
	return (error);
}
