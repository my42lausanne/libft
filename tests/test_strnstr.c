/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strnstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 12:10:05 by davifah           #+#    #+#             */
/*   Updated: 2021/10/16 12:18:30 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

int	test_strnstr(void)
{
	int		i;
	int		j;
	int		len;
	int		error;
	char	tab[7][100];
	char	findstr[10];

	ft_bzero(tab[0], 100);
	ft_bzero(tab[1], 100);
	ft_bzero(tab[2], 100);
	ft_bzero(tab[3], 100);
	ft_bzero(tab[4], 100);
	ft_bzero(tab[5], 100);
	ft_bzero(tab[6], 100);
	ft_memset(tab[0], 't', 99);
	ft_memcpy(tab[1], "BarBar,,BarBarBarBarBarBarBar", 28);
	ft_memcpy(tab[2], "BaBar,,BarBarBarBarBarBaarBar", 28);
	ft_memcpy(tab[3], "BaBaaar,,BarBarBarBarBarBaarBar", 28);
	ft_memcpy(tab[4], "", 0);
	ft_memcpy(tab[5], 0, 0);
	ft_memcpy(tab[5], "yyyyyyyyyyyyyyyyyyyyyyyyyyyyBar", 28);
	error = 0;
	len = -1;
	while (++len < 100)
	{
		i = -1;
		while (++i < 7)
		{
			j = -1;
			while (++j < 5)
			{
				memcpy(findstr, "Bar,", 5);
				findstr[j] = 0;
				if (s_strnstr(tab[i], findstr, len) != ft_strnstr(tab[i], findstr, len))
				//if (strnstr(tab[i], findstr, len) != ft_strnstr(tab[i], findstr, len))
				{
					error = 1;
					printf("Error tab %d, find %s, len %d - og %p - yours %p - text %s\n", i, findstr, len, s_strnstr(tab[i], findstr, len), ft_strnstr(tab[i], findstr, len), ft_strnstr(tab[i], findstr, len));
				}
			}
		}
	}
	return (error);
}
