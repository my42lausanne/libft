/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isprint.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 12:04:56 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/12 11:42:45 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <ctype.h>

int	test_isprint(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 1000)
		if (!is_compare((int) isprint(i), (int) ft_isprint(i)))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", isprint(i), ft_isprint(i), i);
		}
	return (error);
}
