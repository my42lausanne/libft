/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memmove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 12:23:42 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 13:14:10 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>
#include <unistd.h>

int	test_memmove(void)
{
	int		i;
	int		j;
	int		error;
	char	src[50];
	char	src2[50];

	error = 0;
	i = -1;
	while (++i < 49)
	{
		j = 1;
		while (++j < 49 - i)
		{
			ft_bzero(src, 50);
			ft_memset(src, 't', 49);
			ft_bzero(src2, 50);
			ft_memset(src2, 't', 49);
			memmove(&src[i], &src[25], j);
			ft_memmove(&src2[i], &src2[25], j);
			if (strcmp(&src[i], &src2[i]) != 0)
			{
				error = 1;
				printf("ERROR len %d, pos %d - og %s - yours %s\n", j, i, src, src2);
			}
		}
	}
	return (error);
}
