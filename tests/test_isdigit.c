/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isdigit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:27:21 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/12 11:42:02 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <ctype.h>

int	test_isdigit(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 127)
		if (!is_compare((int) isdigit(i), (int) ft_isdigit(i)))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", isdigit(i), ft_isdigit(i), i);
		}
	return (error);
}
