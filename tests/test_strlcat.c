/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlcat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 10:20:21 by davifah           #+#    #+#             */
/*   Updated: 2021/10/16 09:43:41 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

int	test_strlcat(void)
{
	char	src[100];
	char	dest[100];
	char	dest2[100];
	int		error;
	int		i;
	int		j;
	int		k;
	int		r;
	int		r1;

	error = 0;
	i = -1;
	while (++i < 15)
	{
		j = -1;
		while (++j <= 15)
		{
			k = -1;
			while (++k < 15)
			{
				ft_bzero(src, 100);
				ft_bzero(dest, 100);
				ft_bzero(dest2, 100);
				ft_memset(dest, 'y', j);
				ft_memset(dest2, 'y', j);
				ft_memset(src, 't', k);
				
				//must test with strlcat
				r = s_strlcat(dest, src, i);
				//r = strlcat(dest, src, i);
				r1 = ft_strlcat(dest2, src, i);
				if (strcmp(dest, dest2) != 0 || r != r1)
				{
					error = 1;
					printf("ERROR j %d, k %d, len %d, r %d, r1 %d, og %s, yours %s\n", j, k, i, r, r1, dest, dest2);
				}
			}
		}
	}
	return (error);
}
