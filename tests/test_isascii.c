/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isascii.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 11:49:53 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/12 11:42:56 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <ctype.h>

int	test_isascii(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 1000)
		if (!is_compare((int) isascii(i), (int) ft_isascii(i)))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", isascii(i), ft_isascii(i), i);
		}
	return (error);
}
