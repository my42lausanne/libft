/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_atoi.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:25:18 by davifah           #+#    #+#             */
/*   Updated: 2021/10/16 09:18:39 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <stdlib.h>
#include <limits.h>

int	test_atoi(void)
{
	int		error;
	int		i;
	char	str[100];

	error = 0;
	i = -1002;
	while (++i <= 1001)
	{
		ft_bzero(str, 100);
		if (i == -1001)
			sprintf(str, "%d", INT_MIN);
		else if (i == 1001)
			sprintf(str, "%d", INT_MAX);
		else	
			sprintf(str, "%d", i);
		if (atoi(str) != ft_atoi(str))
		{
			error = 1;
			printf("Error str %s - og %d - yours %d\n", str, atoi(str), ft_atoi(str));
		}
		ft_memcpy(&str[1], "at", 2);
		if (atoi(str) != ft_atoi(str))
		{
			error = 1;
			printf("Error str %s - og %d - yours %d\n", str, atoi(str), ft_atoi(str));
		}
		ft_memcpy(&str[1], "-9", 2);
		if (atoi(str) != ft_atoi(str))
		{
			error = 1;
			printf("Error str %s - og %d - yours %d\n", str, atoi(str), ft_atoi(str));
		}
	}
	return (error);
}
