/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strncmp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:26:42 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 17:56:22 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "test.h"

static void	populate_s(char *s)
{
	int	c;

	ft_bzero(s, 100);
	c = -1;
	while (++c + 'a' < 'z')
		s[c] = c + 'a';
}

static int	cmp_compare(int	a, int b)
{
	if ((a == 0 && b == 0) ||
		(a < 0 && b < 0) ||
		(a > 0 && b > 0))
		return (1);
	return (0);
}

int	test_strncmp(void)
{
	int 	error;
	int		i;
	int		j;
	char	s1[100];
	char	s2[100];

	error = 0;
	i = -1;
	while (++i < 30)
	{
		j = -1;
		while (++j < 30)
		{
			populate_s(s1);
			populate_s(s2);
			s1[i] = 0;
			if (!cmp_compare(ft_strncmp(s1, s2, j), strncmp(s1, s2, j)))
			{
				error = 1;
				printf("ERROR s1, nulpos %d, len %d - og %d - yours %d\n", i, j, strncmp(s1, s2, j), ft_strncmp(s1, s2, j));
			}
			if (!cmp_compare(ft_strncmp(s2, s1, j), strncmp(s2, s1, j)))
			{
				error = 1;
				printf("ERROR s2, nulpos %d, len %d - og %d - yours %d\n", i, j, strncmp(s2, s1, j), ft_strncmp(s2, s1, j));
			}
		}
	}
	return (error);
}
