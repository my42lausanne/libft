/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_toupper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 16:19:47 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 16:27:59 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include "ctype.h"

int	test_toupper(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 1000)
		if (toupper(i) != ft_toupper(i))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", toupper(i), ft_toupper(i), i);
		}
	return (error);
}
