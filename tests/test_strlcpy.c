/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlcpy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 15:05:41 by davifah           #+#    #+#             */
/*   Updated: 2022/01/03 15:37:02 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

int	test_strlcpy(void)
{
	char	src[15];
	char	dest[15];
	char	dest2[15];
	int		error;
	int		i;
	int		r;
	int		r1;

	error = 0;
	i = -1;
	while (++i < 15)
	{
		ft_bzero(src, 15);
		ft_bzero(dest, 15);
		ft_bzero(dest2, 15);
		ft_memset(src, 't', 14);

		//must test with strlcat
		r = s_strlcpy(dest, src, i);
		//r = strlcpy(dest, src, i);
		r1 = ft_strlcpy(dest2, src, i);
		if (memcmp(dest, dest2, 15) != 0 || r != r1)
		{
			error = 1;
			printf("ERROR r %d, r1 %d, len %d, og %s, yours %s\n", r, r1, i, dest, dest2);
		}
	}
	return (error);
}
