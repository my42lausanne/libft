/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testmain.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 12:52:26 by dfarhi            #+#    #+#             */
/*   Updated: 2021/10/15 15:35:51 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

int	main(int ac, char **av)
{
	int	testall;
	int	tested;

	tested = 0;
	testall = 0;
	if (ac == 1)
		testall = 1;
	if (testall || strcmp(av[1], "isalpha") == 0)
	{
		tested++;
		if (test_isalpha())
			printf("ERROR: isalpha\n");
	}
	if (testall || strcmp(av[1], "isdigit") == 0)
	{
		tested++;
		if (test_isdigit())
			printf("ERROR: isdigit\n");
	}
	if (testall || strcmp(av[1], "isalnum") == 0)
	{
		tested++;
		if (test_isalnum())
			printf("ERROR: isalnum\n");
	}
	if (testall || strcmp(av[1], "isascii") == 0)
	{
		tested++;
		if (test_isascii())
			printf("ERROR: isascii\n");
	}
	if (testall || strcmp(av[1], "isprint") == 0)
	{
		tested++;
		if (test_isprint())
			printf("ERROR: isascii\n");
	}
	if (testall || strcmp(av[1], "strlen") == 0)
	{
		tested++;
		if (test_strlen())
			printf("ERROR: strlen\n");
	}
	if (testall || strcmp(av[1], "memset") == 0)
	{
		tested++;
		if (test_memset())
			printf("ERROR: strlen\n");
	}
	if (testall || strcmp(av[1], "bzero") == 0)
	{
		tested++;
		if (test_bzero())
			printf("ERROR: bzero\n");
	}
	if (testall || strcmp(av[1], "memcpy") == 0)
	{
		tested++;
		if (test_memcpy())
			printf("ERROR: memcpy\n");
	}
	if (testall || strcmp(av[1], "memmove") == 0)
	{
		tested++;
		if (test_memmove())
			printf("ERROR: memmove\n");
	}
	if (testall || strcmp(av[1], "strlcpy") == 0)
	{
		tested++;
		if (test_strlcpy())
			printf("ERROR: strlcpy\n");
	}
	if (testall || strcmp(av[1], "strlcat") == 0)
	{
		tested++;
		if (test_strlcat())
			printf("ERROR: strlcat\n");
	}
	if (testall || strcmp(av[1], "toupper") == 0)
	{
		tested++;
		if (test_toupper())
			printf("ERROR: toupper\n");
	}
	if (testall || strcmp(av[1], "tolower") == 0)
	{
		tested++;
		if (test_tolower())
			printf("ERROR: tolower\n");
	}
	if (testall || strcmp(av[1], "strchr") == 0)
	{
		tested++;
		if (test_strchr())
			printf("ERROR: strchr\n");
	}
	if (testall || strcmp(av[1], "strrchr") == 0)
	{
		tested++;
		if (test_strrchr())
			printf("ERROR: strrchr\n");
	}
	if (testall || strcmp(av[1], "strncmp") == 0)
	{
		tested++;
		if (test_strncmp())
			printf("ERROR: strncmp\n");
	}
	if (testall || strcmp(av[1], "memchr") == 0)
	{
		tested++;
		if (test_memchr())
			printf("ERROR: memchr\n");
	}
	if (testall || strcmp(av[1], "memcmp") == 0)
	{
		tested++;
		if (test_memcmp())
			printf("ERROR: memcmp\n");
	}
	if (testall || strcmp(av[1], "strnstr") == 0)
	{
		tested++;
		if (test_strnstr())
			printf("ERROR: strnstr\n");
	}
	if (testall || strcmp(av[1], "atoi") == 0)
	{
		tested++;
		if (test_atoi())
			printf("ERROR: atoi\n");
	}

	if (tested == 0 && !testall)
		printf("Name entered as argument is not valid, no tests made\n");
	else
		printf("%d  tests made\n", tested);
}
