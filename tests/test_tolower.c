/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_tolower.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 16:25:25 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 16:28:39 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include "ctype.h"

int	test_tolower(void)
{
	int	i;
	int	error;

	error = 0;
	i = -1;
	while (++i <= 1000)
		if (tolower(i) != ft_tolower(i))
		{
			error = 1;
			printf("Error og %d - yours %d - char %d\n", tolower(i), ft_tolower(i), i);
		}
	return (error);
}
