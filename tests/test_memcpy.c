/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memcpy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 12:00:58 by davifah           #+#    #+#             */
/*   Updated: 2021/10/12 12:11:37 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <string.h>

int	test_memcpy(void)
{
	int		i;
	int		error;
	char	dest[100];
	char	dest2[100];
	char	src[100];

	ft_memset(src, 't', 99);
	error = 0;
	i = -1;
	while (++i < 100)
	{
		ft_bzero(dest, 100);
		ft_bzero(dest2, 100);
		memcpy(dest, src, i);	
		ft_memcpy(dest2, src, i);
		if (strcmp(dest, dest2) != 0)
		{
			error = 1;
			printf("ERROR len %d - og %s - yours %s\n", i, dest, dest2);
		}
	}
	return (error);
}
